package pl.sda.project;

import java.util.Scanner;

public class ProjectTicTacToe {
    public static void main(String[] args) {
        System.out.println("Let's play the game...");
        Scanner sc = new Scanner(System.in);
        String[][] board = new String[3][3];
        boolean isEndingConditionFullfilled;
        boolean isXCurrentPlayer = false;
        boolean isFirstPlayerSignOK = false;

        initializeBoard(board);
        printBoard(board);

        do {
            System.out.println("Choose your sign ( X / O ):");
            String sign = sc.nextLine().toUpperCase();
            if ("X".equals(sign)) {
                isXCurrentPlayer = true;
                isFirstPlayerSignOK = true;
            } else if ("O".equals(sign)) {
                isFirstPlayerSignOK = true;
            } else {
                System.out.println("You typed incorrect sign, try again ( X / O ):");
            }
        } while (!isFirstPlayerSignOK);

        do {
            System.out.println("'" + getCurrentPlayerSign(isXCurrentPlayer) + "' - your time to move, choose the field by typing a number:");
            fillTheField(sc, board, isXCurrentPlayer);
            printBoard(board);
            isEndingConditionFullfilled = checkEndingConditions(board, getCurrentPlayerSign(isXCurrentPlayer));
            isXCurrentPlayer = !isXCurrentPlayer;
        } while (!isEndingConditionFullfilled);
    }

    private static void initializeBoard(String[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = "" + (i + 3 * j + 1);
            }
        }
    }

    private static void printBoard(String[][] board) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (i % 2 != 0 && j % 2 == 0) {
                    System.out.print('-' + " ");
                } else if (i % 2 != 0 && j % 2 != 0) {
                    System.out.print('+' + " ");
                } else if (i % 2 == 0 && j % 2 != 0) {
                    System.out.print('|' + " ");
                } else {
                    System.out.print(board[j / 2][i / 2] + " ");
                }
            }
            System.out.println();
        }
    }

    private static String getCurrentPlayerSign(boolean isXCurrentPlayer) {
        if (isXCurrentPlayer) {
            return "X";
        }
        return "O";
    }

    private static void fillTheField(Scanner sc, String[][] board, boolean isXMovingNow) {
        while (true) {
            String choosenField = sc.nextLine();
            try {
                int choosenFieldNumber = Integer.parseInt(choosenField);

                int i = (choosenFieldNumber - 1) % 3;
                int j = (choosenFieldNumber - 1) / 3;

                if (isChoosenFieldFree(board[i][j])) {
                    if (isXMovingNow) {
                        board[i][j] = "X";
                        return;
                    }
                    board[i][j] = "O";
                    return;
                } else {
                    System.out.println("Te pole jest już zajęte, spróbuj ponownie:");
                }
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                System.out.println("You typed incorrect field number, try again:");
            }
        }
    }

    private static boolean isChoosenFieldFree(String choosenFieldValue) {
        return !"O".equals(choosenFieldValue) && !"X".equals(choosenFieldValue);
    }

    private static boolean checkEndingConditions(String[][] board, String currentPlayer) {
        boolean isWin = false;
        int firstCrossCounter = 0;
        int secondCrossCounter = 0;
        for (int i = 0; i < board.length; i++) {
            int rowCounter = 0;
            int columnCounter = 0;
            for (int j = 0; j < board.length; j++) {
                if (currentPlayer.equals(board[i][j])) {
                    rowCounter++;
                }
                if (currentPlayer.equals(board[j][i])) {
                    columnCounter++;
                }
            }

            if (currentPlayer.equals(board[i][i])) {
                firstCrossCounter++;
            }
            if (currentPlayer.equals(board[i][board.length - 1 - i])) {
                secondCrossCounter++;
            }

            if (rowCounter == board.length || columnCounter == board.length || firstCrossCounter == board.length || secondCrossCounter == board.length) {
                isWin = true;
                break;
            }
        }

        if (isWin) {
            System.out.println("'" + currentPlayer + "' YOU WON!");
            return true;
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (isChoosenFieldFree(board[i][j])) {
                    return false;
                }
            }
        }
        System.out.println("DRAW!!!");
        return true;
    }
}